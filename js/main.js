$(window).ready(function () {

// ================================================= local storage

	function init() {
        if (localStorage["html"]) {
            $('#html').val(localStorage["html"]);
        }
        if (localStorage["css"]) {
            $('#css').val(localStorage["css"]);
        }
        if (localStorage["jquery"]) {
            $('#jquery').val(localStorage["jquery"]);
        }
    }
    init();

	$('.stored').keyup(function () {
	    localStorage[$(this).attr('id')] = $(this).val();
	});

// ================================================= display local storage onload

	var	contents = $('iframe').contents(),
		body = contents.find('body'),
		styleTag = $('<style></style>').appendTo(contents.find('head'));
		// $('<script type="text/javascript" style="display: none !important;">        function cleanJS(js) {          js = js.replace(/location(s+)?=/mi, "");          js = js.replace(/top.location.+=("|")/mi, "");          js = js.replace(/location.replace/mi, "");          js = js.replace(/.submit()/mi, "");          js = js.replace(/fromCharCode/mi, "");          return js;        }        _ogEval        = window.eval;        window.eval    = function(text) {_ogEval(cleanJS(text));};        window.confirm = function(){};        window.prompt  = function(){};        window.open    = function(){};        window.print   = function(){};        window.innerWidth = window.outerWidth; // Fixes browser bug with it innerWidth report 0        // Support hover state for mobile.        window.ontouchstart = function(){};        </script>').appendTo(contents.find('head'));

	body.html($('#html').val());
	styleTag.text( $('#css').val() );

// ================================================= iframe data input

// $('<script></script>').appendTo(contents.find('head'));

function iframebg(){
	if($('#html').val() !== ""){
		$('iframe').animate({backgroundColor: '#fff'});
	}else if($('#css').val() !== ""){
		$('iframe').animate({backgroundColor: '#fff'});
	}else{
		$('iframe').animate({backgroundColor: 'rgba(0, 0, 0, 0.0)'});
	}
} iframebg();

function input() {
		
	$('textarea').keyup(function() {
		iframebg();
		var $this = $(this);
		if ( $this.attr('id') === 'html') {
			body.html( $this.val() );
		} else {
			// it had to be css
			styleTag.text( $this.val() );
		} 
	});

} input();

// ================================================= iframe url input

	$('.urlbttn').click(function(){
		var theurl = $('#url').val();
		$('iframe').attr('src', theurl);
		// $('iframe').attr('src', "http://" + theurl);
	});

// ================================================= iphone grow



	var $iphonoFrame = $('.iphone iframe'),
	 	$iphonoSteel = $('.steel_band'),
	 	$iphonoPlastic = $('.plastic_band'),
	 	$iphonoGlass = $('.glass_face'),
	 	$iphonoScreen = $('.screen'),
	 	$iphonoStage = $('.stage');

	var $iphono = [
	    $iphonoFrame,
	    $iphonoSteel,
	    $iphonoPlastic,
	    $iphonoGlass,
	    $iphonoScreen,
	    $iphonoStage
	]

	var $size = 0;

	$('.phoneToggle5').click(function(e){
		e.preventDefault();
	for (var i = 0; i < $iphono.length; i++) {

		if ($iphonoSteel.height() < 822) {
			$iphono[i].animate({height:$iphono[i].height()+88});
		};//end if
			
	};// end for loop
	
});// end iphone5 click

	$('.phoneToggle4').click(function(e){
			e.preventDefault();
		for (var i = 0; i < $iphono.length; i++) {
		

		if ($iphonoSteel.height() > 740) {
			$iphono[i].animate({height:$iphono[i].height()-88});
		};//end if
			
	};// end for loop


	});// end iphone4 click

});// end ready function


